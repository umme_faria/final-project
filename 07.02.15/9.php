<?php
echo "this is a simple string";
echo "you can also have embedded newlines in strings this way as it is okay to do";
// output: Arnold once said: "i will be back"
echo 'arnold once said: "i\'ll be back" ';
// output : you deleted c:\ *.*?
echo 'You deleted c:\\*.*?';