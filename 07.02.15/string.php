<?php
echo "this is a double quoted string";
?>


<?php

echo 'this is a simple string';

echo 'You can also have embedded newlines in string this way as it is okey to do';

//outputs: Arnold once said: "I\'ll be back"
echo '"I\'ll be back"';
 
//outputs: You deleted C:\*.*?
echo 'You deleted C:\\*.*?';

//outputs: You deleted C:\*.*?
echo 'You deleted C:\*.*?';

//outputs: Variables do not $expand $either
echo 'Variables do not $expand $either';
?>



<?php
$str = <<<EOD
Example of string
spanning multiple lines
using heredoc syntax.
EOD;

echo $str;

?>


<?php

$str = <<<'EOD'
Example of string
spanning multiple lines
using nowdoc syntax.
EOD;

echo $str;

?>
